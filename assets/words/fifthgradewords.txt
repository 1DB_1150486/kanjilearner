せいじか/政治家/politician/seijika
べんごし/弁護士/lawyer/bengoshi
しょうぼうし/消防士/firefighter/shouboushi
けんちくか/建築家/architect/kenchikuka
きょうし/教師/academicteacher/kyoushi
そふ/祖父/grandfather/sofu
そぼ/祖母/grandmother/sobo
きんぞく/金属/metal,metallic/kinzoku
こぜに/小銭/smallchange/kozeni
ふとん/布団/futon/futon
じょうぎ/定規/ruler/jougi
めがね/眼鏡/glasses/megane
ひらがな/平仮名/hiragana characters/hiragana
じょうほう/情報/information/joohoo
ないよう/内容/content/naiyoo
げんざい/現在/now/genzai
かこ/過去/past/kako
すばらしい/素晴らしい/wonderful/subarashii
やさしい/易しい/easy,simple/yasashii
あらわれる/現れる/to appear/arawareru
そなわる/備わる/to be furnished with/sonawaru
ます/増す/toincrease,togrow/masu
ふえる/増える/toincrease,tomultiply/fueru
へる/減る/todecrease/heru
あつい/厚い/thick/atsui
おべんとう/お弁当/lunchbox/obentou
かす/貸す/to lend/kasu
かわいい/可愛い/cute, pretty/kawaii
さらいねん/再来年/the year after next year/sarainen
しつもん/質問/question/shitsumon
じゅぎょう/授業/lesson, class/jugyou
はる/張る/to put something on, to stick/haru
りゅうがくせい/留学生/foreign student/ryuugakusei
placeholder/酸っぱい/sour/suppai
